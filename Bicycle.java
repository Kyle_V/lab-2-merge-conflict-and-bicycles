/**
 * Name: Kyle Veloso (id: 1635447)
 */

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        //validation
        if (numberGears <= 0 || maxSpeed <= 0) {
            throw new IllegalArgumentException("numberGears and maxSpeed must be positive!");
        }
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }


    //override toString
    public String toString() {
        return "Manufacturer: " + this.manufacturer + " | Number of Gears: " + this.numberGears + " | maxSpeed: " + this.maxSpeed + " km/h";
    }
}