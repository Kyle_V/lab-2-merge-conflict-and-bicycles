/**
 * Name: Kyle Veloso (id: 1635447)
 */

public class BikeStore {
    

    public static void main(String[] args) {
        


        //the bike shop, and making bicycle objects
        Bicycle[] bicycleShop = new Bicycle[4];
        bicycleShop[0] = new Bicycle("Bikes r us", 5, 25);
        bicycleShop[1] = new Bicycle("Specialized", 21, 40);
        bicycleShop[2] = new Bicycle("Mountain Made", 15, 50);
        bicycleShop[3] = new Bicycle("Unknown", 2, 10.5);

        //printing all bikes
        for (int i = 0; i < bicycleShop.length; i++) {
            System.out.println(bicycleShop[i]);
        }

    }
}
